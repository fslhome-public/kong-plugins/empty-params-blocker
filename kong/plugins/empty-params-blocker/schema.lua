local typedefs = require "kong.db.schema.typedefs"


return {
  name = "empty-params-blocker",
  fields = {
    {
      -- this plugin will only be applied to Services or Routes
      consumer = typedefs.no_consumer
    },
    {
      -- this plugin will only run within Nginx HTTP and HTTPS module
      protocols = { typedefs.protocols_http, typedefs.protocols_https }
    },
    {
      config = {
        type = "record",
        fields = {
          -- Describe your plugin's configuration's schema here.    
        
            { http_methods_to_check = {
                    type = "array",
                    elements = { type = "string" },
                    default = { "DELETE" },
                },
            },
            { log_denied_queries = {
                    type = "boolean",
                    default = false,
                },
            },
        },
      },
    },
  },
  entity_checks = {
    -- Describe your plugin's entity validation rules
  },
}