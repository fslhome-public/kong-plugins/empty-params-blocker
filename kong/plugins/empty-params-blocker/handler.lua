local BasePlugin = require "kong.plugins.base_plugin"


local kong = kong


local CustomHandler = BasePlugin:extend()


CustomHandler.VERSION  = "0.0.1"
CustomHandler.PRIORITY = 10


function CustomHandler:new()
  CustomHandler.super.new(self, "empty-params-blocker")
end

function hasValue(tbl, value)
    for k, v in ipairs(tbl) do -- iterate table (for sequential tables only)
        if v == value or (type(v) == "table" and hasValue(v, value)) then -- Compare value from the table directly with the value we are looking for, otherwise if the value is table, check its content for this value.
            return true -- Found in this or nested table
        end
    end
    return false -- Not found
end

function tablelength(T)
    local count = 0
    for _ in pairs(T) do count = count + 1 end
    return count
end

function CustomHandler:access(config)
  CustomHandler.super.access(self)

  local query_method = kong.request.get_method()

  local need_check = hasValue(config.http_methods_to_check, query_method)

  if not need_check then
    return
  end

  local nb_args = tablelength(kong.request.get_query())
  if nb_args == 0 then
    return
  end

  if config.log_denied_queries then
    kong.log.warn("query has been denied from ", kong.client.get_forwarded_ip())
  end

  return kong.response.exit(403)
end


return CustomHandler
